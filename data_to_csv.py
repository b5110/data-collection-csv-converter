import paho.mqtt.client as mqtt
import csv

header = ["median_accel_x", "median_accel_y", "median_accel_z", 
          "median_gyro_x", "median_gyro_y", "median_gyro_z", 
          "pith_integral", "pitch_derrivative", "roll_integral", 
          "roll_derrivative", "step"]

broker_address = "broker.mqttdashboard.com"
start_topic = "d_col_begin"
main_data_topic = "mpu6050_data_set"
csv_file_name = input("CSV dosyası ismi: ")
print("MQTT'den başla komutu bekleniyor...")

def on_message_start(client, userdata, message):
    if(str(message.payload.decode("utf-8")) == "start"):
        client.unsubscribe(start_topic)
        client.on_message = on_message_data
        client.subscribe(main_data_topic)
        print("Başlama komutu alındı, topic değişti.")
        with open(csv_file_name + ".csv", "w", encoding="utf-8", newline="\n") as fil:
            writer = csv.writer(fil)
            writer.writerow(header)
            print(csv_file_name + ".csv dosyası oluşturuldu.")
        
    
def on_message_data(client, user_data, message):
    #print("                             Raw Data\n", bytearray(message.payload))
    median_accel_x = int.from_bytes(message.payload[0:2:1], "little", signed=True)
    median_accel_y = int.from_bytes(message.payload[2:4:1], "little", signed=True)
    median_accel_z = int.from_bytes(message.payload[4:6:1], "little", signed=True)
    print("M_accel_x:", median_accel_x, "   M_accel_y:", median_accel_y, " M_accel_z:", median_accel_z, end=" ")
    median_gyro_x = int.from_bytes(message.payload[6:8:1], "little", signed=True)
    median_gyro_y = int.from_bytes(message.payload[8:10:1], "little", signed=True)
    median_gyro_z = int.from_bytes(message.payload[10:12:1], "little", signed=True)
    print("   M_gyro_x:", median_gyro_x, "   M_gyro_y:", median_gyro_y, "  M_gyro_z:", median_gyro_z)
    pitch_integral = int.from_bytes(message.payload[12:14:1], "little", signed=True)
    pitch_derrivative = int.from_bytes(message.payload[14:16:1], "little", signed=True)
    roll_integral = int.from_bytes(message.payload[16:18:1], "little", signed=True)
    roll_derrivative = int.from_bytes(message.payload[18:20:1], "little", signed=True)
    step = int(message.payload[20])
    print("P_integral:", pitch_integral, "  P_derr_sum:", pitch_derrivative, end=" ")
    print(" R_integral:", roll_integral, "  R_derr_sum:", roll_derrivative, end=" ")
    print(" Button_pressed:", step, end="\n\n\n")
    csv_data_row = [median_accel_x, median_accel_y, median_accel_z, 
                    median_gyro_x, median_gyro_y, median_gyro_z,
                    pitch_integral, pitch_derrivative, roll_integral,
                    roll_derrivative, step]
    with open(csv_file_name + ".csv", "a", encoding="utf-8", newline="\n") as fil:
        writer = csv.writer(fil)
        writer.writerow(csv_data_row)


client = mqtt.Client("ESP8266Ahmet_2")
client.on_message = on_message_start
client.connect(broker_address)
client.subscribe(start_topic)
client.loop_forever()
    

